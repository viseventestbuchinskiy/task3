function openPopup() {
    let gallery = document.querySelector(".gallery"),
        popup = document.getElementById("popup"),
        close = document.getElementsByClassName("close")[0],
        like = popup.querySelector(".like span"),
        dislike = popup.querySelector(".dislike span"),
        imageFull = popup.querySelector(".full-image"),
        commentsCount = popup.querySelector(".count span");

    gallery.addEventListener("click", e => {
        let images = gallery.querySelectorAll(".photo");
        for (let i = 0; i < arr.length; i++) {
            if (e.target.id == arr[i].id) {
                popup.style.display = "block";
                popup.setAttribute("data-id", e.target.id);
                like.textContent = arr[i].like;
                dislike.textContent = arr[i].dislike;
                commentsCount.textContent = arr[i].comments.length;
                imageFull.style.backgroundImage = "url(" + arr[i].src + ")";

                loadComments(e.target.id);
                addLike(e.target.id);
            }
        }
    });

    close.onclick = function() {
        popup.style.display = "none";
    };
    window.onclick = function(e) {
        if (e.target == popup) {
            popup.style.display = "none";
        }
    };
}
