// start
window.addEventListener("load", init);

function init(e) {
  let container = document.querySelector("#app"),
    gallery = document.createElement("div");
  gallery.className = "gallery";

  container.appendChild(gallery), (btn = createButton());
  pages = createPages(btn);
  btn.addEventListener("change", btnChangeHandler);

  // append pages to DOM
  for (let i = 0; i < pages.length; i++) {
    gallery.appendChild(pages[i]);
  }

  addPreview(arrCopy);
  openPopup();
  addComment();

}
