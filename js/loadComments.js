function loadComments(imageID) {
    var commentsContainer = document.querySelector(".comments-container");
    commentsContainer.innerHTML = '';
    for (let i = 0; i < arr.length; i++) {
        if (imageID == arr[i].id) {
            for (let j = 0; j < arr[i].comments.length; j++) {
                var commentTemplate = document.querySelector("#clone .comment").cloneNode(true),
                    nickname = commentTemplate.getElementsByClassName("author")[0],
                    date = commentTemplate.getElementsByClassName("date")[0],
                    count = document.querySelector("#popup .count span"),
                    content = commentTemplate.getElementsByClassName("content")[0];
                nickname.innerHTML = arr[i].comments[j].nickname;
                content.innerHTML = arr[i].comments[j].content;
                count.innerHTML = arr[i].comments.length;
                
                date.innerHTML = '7 days ago 4:12 AM';
                commentsContainer.appendChild(commentTemplate);


            }
        }
    }
}