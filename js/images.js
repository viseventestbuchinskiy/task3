var arr = [
    {
        id: getId(),
        src: "images/1.jpg",
        like: 4,
        dislike: 2,
        comments: [
            {
                nickname: "tender 1",
                content: "some comment",
                date: "7 days ago 4:12 AM"
            },
            {
                nickname: "tender 2",
                content: "some comment",
                date: "7 days ago 4:12 AM"
            }
        ]
    },
    {
        id: getId(),
        src: "images/2.jpg",
        like: 3,
        dislike: 6,
        comments: []
    },
    {
        id: getId(),
        src: "images/3.jpg",
        like: 4,
        dislike: 2,
        comments: []
    },
    {
        id: getId(),
        src: "images/4.jpg",
        like: 0,
        dislike: 0,
        comments: []
    },
    {
        id: getId(),
        src: "images/5.jpg",
        like: 0,
        dislike: 0,
        comments: []
    },
    {
        id: getId(),
        src: "images/6.jpg",
        like: 0,
        dislike: 0,
        comments: []
    },
    {
        id: getId(),
        src: "images/7.jpg",
        like: 0,
        dislike: 0,
        comments: []
    },
    {
        id: getId(),
        src: "images/8.jpg",
        like: 0,
        dislike: 0,
        comments: []
    }
];
var arrCopy = arr.slice();

function getId() {
    function id() {
        return (Math.random() * 500).toFixed(0);
    }

    return "" + id() + id() + id();
}
