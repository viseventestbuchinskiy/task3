function createPages(btn) {
  let btnPlace = 1,
    itemsLimit = 9,
    arrClone = [];

  // check how pages on gallery, if 0 = create 1, else add 1++
  let iterationNumber = Math.max((arr.length + btnPlace) / itemsLimit, 1),
    pages = [];
  for (let i = 0; i < iterationNumber; i++) {
    let page = document.createElement("div");
    (page.className = "page"), (arrClone = []);

    if (arr.length > 8) {
      arrClone = arr.splice(0, 9);
    } else {
      arrClone = arr.slice();
      arrClone.push(btn);
    }

    for (let j = 1; j <= arrClone.length; j++) {
      let photo = document.createElement("div");

      photo.className = "photo photo" + j;
      if (arrClone[j - 1].src) {
        photo.style.backgroundImage = "url(" + arrClone[j - 1].src + ")";
        photo.id = arrClone[j - 1].id;
      } else photo.appendChild(arrClone[j - 1]);

      page.appendChild(photo);


    }
    pages.push(page);
  }
  return pages;
}