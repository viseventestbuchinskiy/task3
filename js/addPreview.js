function addPreview(arr) {
  let gallery = document.querySelector(".gallery"),
    preview = document.querySelector(".preview").cloneNode(true);

  gallery.addEventListener("mouseover", e => {
    let images = gallery.querySelectorAll(".photo");
    for (let i = 0; i < arr.length; i++) {
      if (e.target.id == arr[i].id) {
        let like = preview.querySelector(".like span"),
          dislike = preview.querySelector(".dislike span"),
          comments = preview.querySelector(".comments span");
        e.target.appendChild(preview);
        like.textContent = arr[i].like;
        dislike.textContent = arr[i].dislike;
        comments.textContent = arr[i].comments.length;
      }
    }
  });
}
